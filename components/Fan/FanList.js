export default function (color) {
  const img = {
    black: require('../../assets/img/ac.png'),
    red: require('../../assets/img/ac-2.png'),
    blue: require('../../assets/img/ac-3.png'),
  };

  switch (color) {
    case 'Default':
      return {
        img: img.black,
        code: '#000000',
      };

    case 'Red':
      return {
        img: img.red,
        code: '#FF0000',
      };

    case 'Blue':
      return {
        img: img.blue,
        code: '#0028FF',
      };

    default:
      break;
  }
}
