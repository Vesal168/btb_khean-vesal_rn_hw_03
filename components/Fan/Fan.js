import {ActionSheet, Button, Container, Icon, Input} from 'native-base';
import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Animated,
  Easing,
  Dimensions,
  Alert,
  Image
} from 'react-native';
import FanList from './FanList';

var BUTTONS = ['Default', 'Red', 'Blue', 'Cancel'];
var CANCEL_INDEX = 3;

export default class Fan extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fanAnimation: new Animated.Value(0),
      fan: {},
      colorIndex: 0,
    };
  }

  componentDidMount() {
    this.setState({
      fan: FanList('Default'),
    });
  }

  startFan = (duration) => {
    this.state.fanAnimation.setValue(0);
    Animated.loop(
      Animated.timing(this.state.fanAnimation, {
        toValue: 1,
        duration: 450,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();

    if (duration > 0) {
      setTimeout(() => {
        this.stopFan();
      }, duration * 60000);
    }
  };

  stopFan = () => {
    Animated.loop(Animated.timing(this.state.fanAnimation).stop());
  };

  openActionSheet = () => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        title: "Change fan's color ",
      },
      (buttonIndex) => {
        if (BUTTONS[buttonIndex] !== 'Cancel') {
          this.setState({
            fan: FanList(BUTTONS[buttonIndex]),
          });
        }
      },
    );
  };

  setTimer = () => {
    Alert.prompt(
      'Set Timer',
      null,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: (time) => this.startFan(time),
        },
      ],
      'plain-text',
    );
  };

  render() {
    const {fan} = this.state;

    const animationStyles = {
      transform: [
        {
          rotate: this.state.fanAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg'],
          }),
        },
      ],
    };

    return (
      <Container>
        <View>
          <Button 
            transparent
            style={{alignSelf: 'flex-end', backgroundColor: 'red'}}
            onPress={this.openActionSheet}>
            <Text style={{color: 'white'}}>Change Color</Text>
          </Button>
        </View>
        <View style={[styles.fanFrame, {borderColor: fan.code}]}>
          <View>
            <Animated.Image
              source={fan.img}
              style={[animationStyles, {width: 200, height: 200}]}
            />
          </View>
        </View>
        <View
          style={{
            width: 10,
            height: 100,
            backgroundColor: fan.code,
            alignSelf: 'center',
          }}></View>
        <View
          style={{
            width: 100,
            height: 10,
            backgroundColor: fan.code,
            alignSelf: 'center',
          }}></View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 20,
          }}>
          <Button
            onPress={() => this.startFan(0)}
            info
            rounded
            iconLeft
            bordered
            style={styles.buttonStyle}>
            <Text>ON</Text>

          </Button>
          <Button
            onPress={this.stopFan}
            danger
            iconLeft
            bordered
            rounded
            style={styles.buttonStyle}>
            <Text>OFF</Text>
           
          </Button>

          <Button
            onPress={this.setTimer}
            warning
            iconLeft
            bordered
            rounded
            style={styles.buttonStyle}>
            <Text>TIMER</Text>
           
          </Button>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  buttonStyle: {
    width: Dimensions.get('window').width / 4,
    justifyContent: 'space-around',
  },
  fanFrame: {
    borderWidth: 5,
    width: 300,
    height: 300,
    borderRadius: 200,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
