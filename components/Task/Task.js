import {
  Body,
  Button,
  Card,
  CardItem,
  Form,
  Icon,
  Input,
  Item,
  Label,
  Text,
} from 'native-base';
import React, {Component} from 'react';
import {View, StyleSheet, TouchableHighlight} from 'react-native';
import {SwipeListView} from 'react-native-swipe-list-view';

class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskList: [
        {
          key: '1',
          title: 'Task 1',
          description: 'Do Nothing !',
        },
      ],
      title: '',
      description: '',
      isTitleError: false,
      isDescriptionError: false,
    };
  }

  handleAdd = () => {
    const {taskList, title, description} = this.state;
    let id = Math.random().toString();

    if (title === '') {
      this.setState({
        isTitleError: true,
      });
    } else {
      this.setState({
        isTitleError: false,
      });
    }
    if (description === '') {
      this.setState({
        isDescriptionError: true,
      });
    } else {
      this.setState({
        isDescriptionError: false,
      });
    }
    if (title !== '' && description !== '') {
      this.setState({
        taskList: taskList.concat({
          key: id,
          title: title,
          description: description,
        }),
        title: '',
        description: '',
        isTitleError: false,
        isDescriptionError: false,
      });
    }
  };

  handleDelete = (rowMap) => {
    let arr = this.state.taskList;
    arr.splice(rowMap, 1);
    this.setState({
      taskList: arr,
    });
  };

  render() {
    const {isTitleError, isDescriptionError} = this.state;
    return (
      <View>
        <Form>
          <Item floatingLabel error={isTitleError}>
            <Label>Title</Label>
            <Input
              value={this.state.title}
              onChangeText={(title) =>
                this.setState({
                  title: title,
                })
              }
            />
            {isTitleError ? <Icon name="close-circle" /> : null}
          </Item>

          <Item floatingLabel error={isDescriptionError}>
            <Label>Description</Label>
            <Input
              value={this.state.description}
              onChangeText={(des) =>
                this.setState({
                  description: des,
                })
              }
            />
            {isDescriptionError ? <Icon name="close-circle" /> : null}
          </Item>
        </Form>
        <Button block style={{marginTop: 15}} onPress={this.handleAdd}>
          <Text>SAVE</Text>
        </Button>

        <SwipeListView
          style={{marginTop: 10}}
          disableRightSwipe
          data={this.state.taskList}
          renderItem={({item}) => (
            <Card>
              <CardItem header>
                <Text>{item?.title}</Text>
              </CardItem>
              <CardItem style={{marginTop: -15}}>
                <Body
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text numberOfLines={1} ellipsizeMode="tail">
                    {item?.description}
                  </Text>
                </Body>
              </CardItem>
            </Card>
          )}
          renderHiddenItem={(rowData, rowMap) => (
            <View style={styles.rowBack}>
              <TouchableHighlight
                onPress={() => {
                  rowMap[rowData.item.key].closeRow();
                  this.handleDelete(rowData.index);
                }}
                style={[styles.backRightBtn, styles.backRightBtnRight]}>
              </TouchableHighlight>
            </View>
          )}
          rightOpenValue={-75}
          useNativeDriver
          useFlatList
          closeOnRowOpen
          nestedScrollEnabled
        />
      </View>
    );
  }
}

export default Task;

const styles = StyleSheet.create({
  rowBack: {
    alignItems: 'center',
    backgroundColor: '#DDD',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    margin: 5,
    borderRadius: 5,
  },
  backRightBtn: {
    alignItems: 'flex-end',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    paddingRight: 17,
  },

  backRightBtnRight: {
    backgroundColor: 'red',
    right: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
});
