import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Root,
} from 'native-base';
import Task from './components/Task/Task';
import Fan from './components/Fan/Fan';
import Home from './components/Home/Home';

const App = () => {
  const [content, setContent] = useState(<Home />);
  const [isHome, setHome] = useState(true);
  const [isTask, setTask] = useState(false);
  const [isFan, setFan] = useState(false);

  return (
    <Root>
      <Container>
        <Header>
          <Left>
            <Button transparent>
            </Button>
          </Left>
          <Body>
            <Title>Animation</Title>
          </Body>
          <Right>
            <Button transparent>
            </Button>
          </Right>
        </Header>

        <Content contentContainerStyle={{marginHorizontal: 10, flex: 1}}>
          {content}
        </Content>
        <Footer>
          <FooterTab>
            <Button
              icon
              active={isHome}
              onPress={() => {
                setContent(<Home />);
                setHome(true);
                setTask(false);
                setFan(false);
              }}>
          
              <Text>Home</Text>
            </Button>
            <Button
              icon
              active={isTask}
              onPress={() => {
                setContent(<Task />);
                setHome(false);
                setTask(true);
                setFan(false);
              }}>
              
              <Text>Task</Text>
            </Button>
            <Button
              icon
              active={isFan}
              onPress={() => {
                setContent(<Fan />);
                setHome(false);
                setTask(false);
                setFan(true);
              }}>
             
              <Text>Fan</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    </Root>
  );
};

export default App;

const styles = StyleSheet.create({});
